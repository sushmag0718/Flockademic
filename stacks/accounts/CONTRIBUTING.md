The accounts stack is responsible for, well... User accounts.

The setup at the time of writing is rather unconventional though. Since we want to lower the barrier to entry and have the user to be able to get going as soon as possible, we actually generate an account for them behind the screens the moment they do anything requiring an account. This is done by a call to this stack, which generates a UUID and returns a JWT that can be used to sign actions performed for that account, and a refresh token to obtain new JWTs.

This account is bound to the local storage of the machine the user is at at the moment. This is of course rather fragile and not that useful if the user wants to switch devices. The user can therefore, at the time of writing, link their ORCID to create a new master account, to which the auto-generated account will be linked. Alternatively, if a master account already existed for that ORCID, the auto-generated account will be linked to that.

Thus, for everything beyond the user's first steps, we can check the JWT for the presence of a master account as well.

Potentially, we might look into generating master accounts for email addresses as well, but then we'll have to figure out how to merge multiple master accounts - especially w.r.t. the master accounts known in independent stacks, which will also need to know that they're equivalent.