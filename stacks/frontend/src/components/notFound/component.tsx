require('./styles.scss');

import * as React from 'react';
import * as ReactGA from 'react-ga';
import { Link, RouteComponentProps } from 'react-router-dom';

import { PageMetadata } from '../pageMetadata/component';

export class NotFound extends React.Component<RouteComponentProps<{}> | undefined, {}> {
  public componentDidMount() {
    ReactGA.event({
      action: 'Not found',
      category: '404 Page Not Found',
    });
  }

  public render() {
    return (
      <div>
        <section className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                Page not found
                <PageMetadata url={this.props.match.url} title="Page not found"/>
              </h1>
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container content">
            <h2>Aw shucks, nothing here!</h2>
            <p>
              Although there might have been something here at some time, there is nothing special here as we speak.
            </p>
            <p>You might want to try <Link to="/">the homepage</Link>.</p>
            <blockquote>If a page exists but no one is around to see it, is it really there?</blockquote>
          </div>
        </section>
      </div>
    );
  }
}
